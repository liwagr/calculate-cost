/*global describe, it*/
const unexpected = require('unexpected');

const calculateCost = require('./calculateCost');

const expect = unexpected.clone();

describe('calculateCost module', () => {
    it('should return the proper cost when using selection dataType', () => {
        const model = {
            decisionTreeStep: [{
                attributeType: ['Material'],
                selectionValue: ['Concrete'],
                formula: [{
                    formulaExpression: ['Length * 1000']
                }]
            }]
        };
        const asset = {
            attributes: {
                Material: 'Concrete',
                Length: 5
            }
        };

        return expect(calculateCost(asset, model), 'to be fulfilled with', 5000);
    });

    it('should return the proper cost when using decimal dataType', () => {
        const model = {
            decisionTreeStep: [{
                attributeType: ['Length'],
                range: [{
                    minimumValue: [0],
                    maximumValue: [100]
                }],
                formula: [{
                    formulaExpression: ['Length * 1000']
                }]
            }]
        };
        const asset = {
            attributes: {
                Material: 'Concrete',
                Length: 5
            }
        };

        return expect(calculateCost(asset, model), 'to be fulfilled with', 5000);
    });

    it('should return the proper cost when decisionTreeStep is nested', () => {
        const model = {
            decisionTreeStep: [{
                attributeType: ['Material'],
                selectionValue: ['Concrete'],
                decisionTreeStep: [{
                    attributeType: ['Diameter'],
                    range: [{
                        minimumValue: [0],
                        maximumValue: [5]
                    }],
                    decisionTreeStep: [{
                        attributeType: ['Length'],
                        range: [{
                            minimumValue: [0],
                            maximumValue: [100]
                        }],
                        formula: [{
                            formulaExpression: ['Length * 1000 * (Diameter + 5)']
                        }]
                    }, {
                        attributeType: ['Length'],
                        range: [{
                            minimumValue: [100],
                            maximumValue: [1000]
                        }],
                        formula: [{
                            formulaExpression: ['Length * 500 * (Diameter + 5) + 100000']
                        }]
                    }]
                }, {
                    attributeType: ['Diameter'],
                    range: [{
                        minimumValue: [5],
                        maximumValue: [10]
                    }],
                    decisionTreeStep: [{
                        attributeType: ['Length'],
                        range: [{
                            minimumValue: [0],
                            maximumValue: [100]
                        }],
                        formula: [{
                            formulaExpression: ['Length * 1000 * (Diameter + 2)']
                        }]
                    }, {
                        attributeType: ['Length'],
                        range: [{
                            minimumValue: [100],
                            maximumValue: [1000]
                        }],
                        formula: [{
                            formulaExpression: ['Length * 500 * (Diameter + 2) + 100000']
                        }]
                    }]
                }]
            }]
        };
        const asset = {
            attributes: {
                Material: 'Concrete',
                Diameter: 5,
                Length: 200
            }
        };

        return expect(calculateCost(asset, model), 'to be fulfilled with', 1100000);
    });

    it('should return the proper cost if range is open-ended', () => {
        const model = {
            decisionTreeStep: [{
                attributeType: ['Length'],
                range: [{
                    minimumValue: [10]
                }],
                formula: [{
                    formulaExpression: ['Length * 1000']
                }]
            }]
        };
        const asset = {
            attributes: {
                Material: 'Concrete',
                Length: 50
            }
        };

        return expect(calculateCost(asset, model), 'to be fulfilled with', 50000);
    });

    it('should return the proper cost if attribute name has a space', () => {
        const model = {
            decisionTreeStep: [{
                attributeType: ['Depth (m)'],
                range: [{
                    minimumValue: [10]
                }],
                formula: [{
                    formulaExpression: ['Length * 1000']
                }]
            }]
        };
        const asset = {
            attributes: {
                'Depth (m)': 10,
                Length: 50
            }
        };

        return expect(calculateCost(asset, model), 'to be fulfilled with', 50000);
    });

    it('should be rejected if attributes do not match', () => {
        const model = {
            decisionTreeStep: [{
                attributeType: ['Material'],
                selectionValue: ['Concrete'],
                formula: [{
                    formulaExpression: ['Length * 1000']
                }]
            }]
        };
        const asset = {
            attributes: {
                Material: 'PVC',
                Length: 5
            }
        };

        return expect(calculateCost(asset, model), 'to be rejected');
    });
});
