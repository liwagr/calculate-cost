const FormulaParser = require('hot-formula-parser').Parser;

const replaceVariables = require('./replaceVariables');

const decideFormula = (model) => {
    const getFormula = (conditions, attributes) => {
        let conditionMet = false;
        let i = 0;

        while (!conditionMet && i < conditions.length) {
            const condition = conditions[i];
            const { attributeType, selectionValue, range } = condition;
            const attributeValue = attributes[attributeType[0]];

            if (selectionValue !== undefined && attributeValue === selectionValue[0]) {
                conditionMet = true;
            } else if (range !== undefined) {
                const minimumValue = range[0].minimumValue;
                const maximumValue = range[0].maximumValue;

                if (minimumValue && attributeValue >= minimumValue[0] &&
                        (maximumValue && attributeValue <= maximumValue[0] || maximumValue === undefined)) {
                    conditionMet = true;
                } else if (maximumValue && attributeValue <= maximumValue[0] &&
                        (minimumValue && attributeValue >= minimumValue[0] || minimumValue === undefined)) {
                    conditionMet = true;
                }
            }

            if (!conditionMet) {
                i += 1;
            }
        }

        if (conditionMet) {
            if (conditions[i].formula) {
                return conditions[i].formula[0].formulaExpression[0];
            } else if (conditions[i].decisionTreeStep) {
                return getFormula(conditions[i].decisionTreeStep, attributes);
            }
        }
    };

    return (attributes) => {
        const parser = new FormulaParser();
        const formula = getFormula(model.decisionTreeStep, attributes);
        const attributeKeys = Object.keys(attributes);

        if (!formula) {
            return null;
        }

        // FormulaParser does not handle variable names with spaces
        // so replace tokens with simple variables
        const replacement = replaceVariables(formula, attributeKeys);

        attributeKeys.forEach((key) => {
            parser.setVariable(replacement.variables[key], attributes[key]);
        });

        const result = parser.parse(replacement.result);

        if (!result.error) {
            return result.result;
        } else {
            return null;
        }
    };
};

module.exports = (asset, model) => {
    return new Promise((resolve, reject) => {
        const calculate = decideFormula(model);
        const cost = calculate({ ...asset.attributes });

        if (cost === null) {
            return reject(new Error('InvalidAttributes'));
        } else {
            resolve(cost);
        }
    });
};
