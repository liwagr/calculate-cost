const replacementVariables = [];

for (let i = 97; i < 123; i += 1) {
    replacementVariables.push(String.fromCharCode(i));
}

module.exports = (str, variables) => {
    const replacedVariables = {};
    const result = variables.reduce((accum, variable, index) => {
        replacedVariables[variable] = replacementVariables[index];

        // To replace all occurrences of `variable` in string
        return accum.split(variable).join(replacementVariables[index]);
    }, str);

    return {
        result,
        variables: replacedVariables
    };
};
