const util = require('util');
const fsModule = require('fs');
const pathModule = require('path');
const xml2js = require('xml2js');

class ModelReader {
    constructor(dataPath = '.') {
        this.dataPath = dataPath;
    }

    getAll(stream) {
        const readDir = util.promisify(fsModule.readdir);

        // Get list of all models in dataPath directory and read each one at a time
        // Write model to stream as it's fetched to prevent excessive memory buffering
        return readDir(this.dataPath)
        .then((filenames) => {
            const xmlFiles = filenames.filter((name) => /\.xml$/.test(name));

            stream.write('[');
            const readFiles = () => {
                if (xmlFiles.length) {
                    const filename = xmlFiles.pop();

                    return this.getModel(filename.slice(0, -4))
                    .then((model) => {
                        stream.write(JSON.stringify({
                            code: model.assetType[0].$.Code,
                            attributes: model.assetAttribute.map(({ attributeType }) => attributeType[0])
                        }));
                        // Add a comma after each model JSON except the last
                        if (xmlFiles.length) {
                            stream.write(',');
                        }
                    })
                    .then(readFiles);
                }
            };

            return readFiles();
        })
        .then(() => {
            stream.write(']');
        });
    }

    getModel(modelName) {
        const readFile = util.promisify(fsModule.readFile);
        const opts = {
            preserveChildrenOrder: true
        };

        return readFile(pathModule.resolve(this.dataPath, `${modelName}.xml`), 'utf8')
        .then((content) => {
            return new Promise((resolve, reject) => {
                xml2js.parseString(content, opts, (err, result) => {
                    if (err) {
                        return reject(err);
                    }

                    return resolve(result.model);
                });
            });
        });
    }
}

module.exports = ModelReader;
