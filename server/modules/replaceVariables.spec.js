/*global describe, it*/
const unexpected = require('unexpected');
const replaceVariables = require('./replaceVariables');

const expect = unexpected.clone();

describe('replaceVariables module', () => {
    it('should replace variable in string', () => {
        const replacement = replaceVariables('Length * 10', ['Length']);

        expect(replacement.result, 'to equal', 'a * 10');
        expect(replacement.variables, 'to satisfy', { 'Length': 'a' });
    });

    it('should replace multiple instances of variable in string', () => {
        const replacement = replaceVariables('Length * Length * 10', ['Length']);

        expect(replacement.result, 'to equal', 'a * a * 10');
        expect(replacement.variables, 'to satisfy', { 'Length': 'a' });
    });

    it('should replace multiple variables in string', () => {
        const replacement = replaceVariables('Length * Width * 10', ['Length', 'Width']);

        expect(replacement.result, 'to equal', 'a * b * 10');
        expect(replacement.variables, 'to satisfy', {
            'Length': 'a',
            'Width': 'b'
        });
    });
});
