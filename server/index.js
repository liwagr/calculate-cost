const express = require('express');
const pathModule = require('path');

const handlers = require('./handlers');
const ModelReader = require('./modules/ModelReader');
const calculateCost = require('./modules/calculateCost');

const app = express();
const modelReader = new ModelReader(pathModule.resolve(__dirname, 'data'));

app.use('/api', handlers(modelReader, calculateCost));

app.listen(3000);
