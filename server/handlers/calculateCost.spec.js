/*global describe, beforeEach, it*/

const unexpected = require('unexpected');
const sinon = require('sinon');
const unexpectedSinon = require('unexpected-sinon');
const unexpectedExpress = require('unexpected-express');
const express = require('express');
const bodyParser = require('body-parser');

const calculateCost = require('./calculateCost');

describe('calculateCost handler', () => {
    const expect = unexpected.clone()
    .installPlugin(unexpectedExpress)
    .installPlugin(unexpectedSinon);

    beforeEach(function () {
        this.app = express().use(bodyParser.json());
    });

    it('should respond with 400 if assets not provided', function () {
        const modelReader = {};
        const costCalculator = () => {};

        return expect(this.app.use(calculateCost(modelReader, costCalculator)), 'to yield exchange', {
            request: {
                url: '/',
                method: 'POST',
                body: {}
            },
            response: {
                statusCode: 400
            }
        });
    });

    it('should respond with costs', function () {
        const modelReader = {
            getModel: sinon.stub().returns(Promise.resolve())
        };
        const costCalculator = sinon.stub().returns(Promise.resolve(1000));

        return expect(this.app.use(calculateCost(modelReader, costCalculator)), 'to yield exchange', {
            request: {
                url: '/',
                method: 'POST',
                body: {
                    assets: [{
                        id: '1234',
                        modelCode: 'pipe',
                        attributes: {
                            Length: 100
                        }
                    }]
                }
            },
            response: {
                body: [{
                    id: '1234',
                    cost: 1000
                }]
            }
        });
    });
});
