const createError = require('http-errors');

module.exports = (modelReader, calculateCost) => {
    return (req, res, next) => {
        const { assets } = req.body;
        const costs = [];

        if (!Array.isArray(assets)) {
            return next(createError(400, 'No assets provided'));
        }

        // Async asset cost calculation
        const calculateAssetCost = () => {
            if (!assets.length) {
                return costs;
            }

            const asset = assets.pop();

            return modelReader.getModel(asset.modelCode)
            .then((model) => {
                return calculateCost(asset, model);
            })
            .then((cost) => {
                costs.push({
                    id: asset.id,
                    cost
                });
                return calculateAssetCost();
            });
        };

        calculateAssetCost()
        .then((costs) => {
            res.send(costs);
        })
        .catch(() => {
            next(createError(500, 'Could not calculate costs'));
        });
    };
};
