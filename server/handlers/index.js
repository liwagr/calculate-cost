const express = require('express');
const bodyParser = require('body-parser');

const allModels = require('./allModels');
const calculateCost = require('./calculateCost');

module.exports = (modelReader, assetCalculator) => {
    const app = express();

    app.use(bodyParser.json());
    app.get('/models', allModels(modelReader));
    app.get('/calculateCost', calculateCost(modelReader, assetCalculator));

    return app;
};
