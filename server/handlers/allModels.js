module.exports = (modelReader) => {
    return (req, res, next) => {
        res.set('Content-type', 'application/json');

        modelReader.getAll(res)
        .then(() => {
            res.end();
        })
        .catch(next);
    };
};
